import copy
lessons_names = ["maths", "Django", "Python",
                 "Angular", "Node.js", "Flutter", "Bdd", "Random"]
print(lessons_names)
print(type(lessons_names))

lessons_durations = [2, 4, 5.5, 4, 5]
print(lessons_durations)
print(type(lessons_durations))

# Indiçage
print(lessons_names[1])
print(lessons_names[3])
print(lessons_names[-2])


# opérations

full_list = lessons_names + lessons_durations
print(full_list)

many_lessons = lessons_names*2
print(many_lessons)

lessons_names.append('IA')
print(lessons_names)


# Tranches


some_lessons = lessons_names[1:3]
# tranche de lessons_names de 1 à (3-1) [n,m-1]
print(some_lessons)
some_lessons2 = lessons_names[:3]
print(some_lessons2)
some_lessons3 = lessons_names[2:]
print(some_lessons3)
# tranches / pas
some_lessons_step = lessons_names[1:9:3]
print(some_lessons_step)

# fonctions de listes len(), range(), list()

print(len(lessons_names))
new_list = list(range(10))
new_list2 = list(range(10, 16))
print(new_list)
print(new_list2)
new_list_steps = list(range(10, 0, -2))
print(new_list_steps)


# Listes imbriquées

groupe_1 = ['person1', 'person2', 'person3', 'person2', 'person3']
groupe_2 = ['person3', 'person4', 'person5']
groupe_3 = ['person6', 'person7', 'person8']

salle_classe = [groupe_1, groupe_2, groupe_3]
print(salle_classe)
print(salle_classe[-1][-1])

# méthodes list
# insert(post,value)

groupe_1.insert(0, 'Another person')
print(groupe_1)
groupe_1.append('a new person')
print(groupe_1)

# del
del groupe_1[0]
print(groupe_1)

# remove
groupe_1.remove('person2')
groupe_1.remove('person2')

print(groupe_1)

# clear
# groupe_1.clear()
print(groupe_1)

# count
number_of_persons_3 = groupe_1.count("person3")
print(number_of_persons_3)

# extend

groupe_1.extend(groupe_2)
print(groupe_1)

print(salle_classe.count("person3"))

# copie de liste
'''
print("********")
print(groupe_3)
groupe_4 = groupe_3
print(groupe_4)
groupe_4[0] = "nobody"
print(groupe_4)
print(groupe_3)
'''
groupe_4 = copy.deepcopy(groupe_3)
print(groupe_3)
print(groupe_4)
groupe_4[0] = "nobody"
print(groupe_4)
print(groupe_3)

# Exercices

""" 1. Jours de la semaine

Constituez une liste semaine contenant les 7 jours de la semaine.

À partir de cette liste, comment récupérez-vous seulement les 5 premiers jours de la semaine d'une part, et ceux week-end d'autre part ?
Utilisez pour cela l'indiçage.
Cherchez un autre moyen pour arriver au même résultat(en utilisant un autre indiçage).

Trouvez deux manières pour accéder au dernier jour de la semaine.

Inversez les jours de la semaine en une commande.
"""
semaine = ['lundi', 'mardi', 'mercredi',
           'jeudi', 'vendredi', 'samedi', 'dimanche']
print(semaine[:5])
print(semaine[5:])

print(semaine[-2:])
print(semaine[:-2])

print(semaine[-1])
print(semaine[6])
print(semaine[len(semaine)-1])

# semaine.reverse()
print(semaine[::-1])
print(list(reversed(semaine)))


"""
2 Saisons

Créez 4 listes hiver, printemps, ete et automne contenant les mois correspondants à ces saisons. Créez ensuite une
liste saisons contenant les listes hiver, printemps, ete et automne. Prévoyez ce que renvoient les instructions suivantes,
puis vérifiez-le dans l'interpréteur:

saisons[2]
saisons[1][0]
saisons[1:2]
saisons[:][1]. Comment expliquez-vous ce dernier résultat ?

"""

summer = ['juin', 'juillet', 'aout']
fall = ['septembre', 'octobre', 'novembre']
winter = ['decembre', 'janvier', 'fevrier']
spring = ['mars', 'avril', 'mai']

saisons = [summer, fall, winter, spring]
print(saisons)
print(saisons[2])
print(saisons[1][0])
print(saisons[1])
print(saisons[1:2])
print(saisons[:][1])


"""
3 Table de multiplication par 9

Affichez la table de multiplication par 9 en une seule commande avec les instructions range() et list().

4 Nombres pairs

Répondez à la question suivante en une seule commande. Combien y a-t-il de nombres pairs dans l'intervalle[2, 10000] inclus ? """

print(list(range(0, 82, 9)))
print(len(list(range(2, 10001, 2))))
