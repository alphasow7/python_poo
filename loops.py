import statistics

groupe_1 = ['person1', 'person2', 'person3', 'person2', 'person3']

'''
for student in groupe_1:
    print(student)

print(student)
'''

'''
for i in range(len(groupe_1)):
    print(groupe_1[i])
'''
i = 0
while i < len(groupe_1):
    print(groupe_1[i])
    i = i+1

i = 0
while i != 29:
    i = int(input('devinez un nombre ! :'))
else:
    print('Bravo ! vous avez deviné le nombre !')


'''

Jours de la semaine

Constituez une liste semaine contenant le nom des sept jours de la semaine.

En utilisant une boucle, écrivez chaque jour de la semaine ainsi que les messages suivants:

Au travail s'il s'agit du lundi au jeudi;
Chouette c'est vendredi s'il s'agit du vendredi;
Repos ce week-end s'il s'agit du samedi ou du dimanche.
Ces messages ne sont que des suggestions, vous pouvez laisser libre cours à votre imagination.

'''
semaine = ['lundi', 'mardi', 'mercredi',
           'jeudi', 'vendredi', 'samedi', 'dimanche']

for jour in semaine:
    if jour == 'lundi' or jour == 'mardi' or jour == 'mercredi' or jour == 'jeudi':
        print(f"{jour} : Au travail s'il s'agit du lundi au jeudi;")
    elif jour == 'vendredi':
        print(f"{jour} : Chouette c'est vendredi !;")
    else:
        print(f"{jour} : Repos ce week-end")

print("*******")

for jour in semaine:
    if semaine[:4].count(jour) > 0:
        print(f"{jour} : Au travail s'il s'agit du lundi au jeudi;")
    elif semaine[5:].count(jour) > 0:
        print(f"{jour} : Repos ce week-end")
    else:
        print(f"{jour} : Chouette c'est vendredi !;")


'''
Notes et mention d'un étudiant

Voici les notes d'un étudiant: 14, 9, 13, 15 et 12. Créez un script qui affiche la note maximum
(utilisez la fonction max()), la note minimum(utilisez la fonction min()) et qui calcule la moyenne.

Affichez la valeur de la moyenne avec deux décimales.
Affichez aussi la mention obtenue sachant que la mention est « passable » si la moyenne est entre 10 inclus et 12 exclus,
« assez bien » entre 12 inclus et 14 exclus et « bien » au-delà de 14.
'''

notes = [14, 9, 13, 15, 12]
max_note = max(notes)
print(max_note)
min_note = min(notes)
print(min_note)

somme = 0

for n in notes:
    somme = somme + n

moyenne = somme/len(notes)
print('{:.2f}'.format(moyenne))

if moyenne >= 10 and moyenne < 12:
    print("Passable")
elif moyenne >= 12 and moyenne < 14:
    print('Assez bien')
elif moyenne >= 14:
    print("Bien")
else:
    print("pas asssez")

test = 5.9787
print('%.2f' % (test))
print(round(test, 2))

moyennes2 = statistics.mean(notes)
print(moyennes2)


'''


5.4.1 Boucles de base

Soit la liste['vache', 'souris', 'levure', 'bacterie']. Affichez l'ensemble des éléments de cette liste(un élément par ligne) de trois manières différentes(deux avec for et une avec while).
'''
elements = ['vache', 'souris', 'levure', 'bacterie']
for i in range(len(elements)):
    print(elements[i])


print("---------")


for element in elements:
    print(element)

print("---------")

i = 0
while i < len(elements):
    print(elements[i])
    i = i+1

'''
Triangle

Créez un script qui dessine un triangle comme celui-ci:

*
**
***
****
*****
******
*******
********
*********
**********
'''

for i in range(10):
    print('*'*(i+1))

for i in range(10):
    for j in range(i+1):
        print('*', end='')
    print()

for i in range(10):
    etoiles = ''
    for j in range(i+1):
        etoiles = etoiles+'*'
    print(etoiles)


'''
5.4.8 Triangle inversé

Créez un script qui dessine un triangle comme celui-ci:


**********
*********
********
*******
******
*****
****
***
**
*
'''

for i in range(10, 0, -1):
    print('*'*i)


'''

5.4.9 Triangle gauche

Créez un script qui dessine un triangle comme celui-ci:

         *
        **
       ***
      ****
     *****
    ******
   *******
  ********
 *********
**********

'''

for i in range(10):
    print(' '*(10-i-1)+'*'*i)

'''

5.4.10 Pyramide

Créez un script pyra.py qui dessine une pyramide comme celle-ci :


         *
        ***
       *****
      *******
     *********
    ***********
   *************
  ***************
 *****************
*******************



'''

for i in range(10):
    print(' '*(10-i-1)+'*'*((2*i)+1))
